﻿using iText.Kernel.Pdf;
using iText.Kernel.Utils;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ElementPostJobPDF
{
    public static class Progam
    {
        public static void Main()
        {
            string spireKey = @"dDyyuax4lJx2OJ1PwQEAXFthjqEJJMvm0AnLgRaS/V3oTQHujNi6djynG38LugiHk+KxwfYU9PePMj11r//WbUlrcwG6a/zu2VRQc/FjEor1nNQg4RvBJVceXdgsk8BqqrKpoyuEPYNGD0U6B1PWItYCArbTVdx3pAvuJzMzzX5HCqE0PJ6SvOTbBAswqw6SU//N/78WXf4v2B6wswUHjfXziqdXbBR4keFqAbFQahRrCYmudmBa6LNCDenDb0Q+hqYXAQPurvfhlfi9hKGEeN1WkZ67EvzkJdQVN+8fAFzqFohI9kL53U5p5D3pNmf42x6Nl00FEETU5ZzdVnsPGukoxmycVvRU7EIkt5X1Twh1zsXCHtmgsG5JOoN/0rX4RxshAF/L5xJ3k3CGA5waXB59oDOe3kbJYPDUpbvfdA6JDkOf7wE+sMcuKY3paNDhqcqlby1Q/E0ycrL394ilZ40xYxqbz5Ii62koMJVpYErOaYy5jnMjjV79Kn+gdA+joqAALn6zQ8+q7wFIeletXnnHHDKdlwZYVepv/JtJYwRaWEZngvvDoylD/3g4kv3jCRDb5RGTe2NM6qMYJJTmK7Aaln7jmMrbxMXijFqqI9pPME5TN0s0nVZ5bDlbCJrqFxuVGiEcrlcq9UOwBZYr9IK/LDtNt78qYZ8TLDK5P7DY6OvLNWvIWRgj0LFCwDHk/V6vLVnRupf3BEm0xruSilLAYxFrvmTHtbbSwHUA49eoGHEg7NXUtaWDPwgPE2n5PtM0UgX73ZoXPj+nn4a9IelIsQcgbXB1Bb0u8OcsT4uYBOGCgiezQbUNBTR9wKtg+g6s4z8h1GYGJ9CG9TSXaZ+G9tTUusyprNem59f28GLDgTOiY75KZLKQ9ZcxeDkbxO7WyfBlVeHaf19Ga00DBlBgfGBjTaaN1FcI4gNM+n0EVBt0riJjkwlHmpk2yHwBcIG60whFR78c5PkznvB9zfP7y60r1oKBmmPYJEe2humUgKyPb+NsTlkl+KwyXCrex4Av6XAheFeYuFle8nxpl6AW4I4+YQ5o0L1gxr6j+ievl/yY5I9HkwGtGoDV4olrB1tp8MOZqF2ch/0/BcBS90+avidS7UAQgga2+5GbxGqb6+zaOCsw8gTTolFm+ladcWaT1aGkIsTxrn+6luw/aHQWGwi4curx7D8FTK+pUi8JYry+AcDp/UpbdgD1zeKRrRDXSpDXXmcFcNyWOyj80IJfiNvx0GzqkaIOYBEgD4biBndFBS3BF83/MJtKvZKRoUQS5EffY1FkjxyIJD+/IqtdrUrmi8tZ9EB9DssTGESndlmqBF8SFJ1EVxqm3N1dxZjRn8+WtcbfBMc7KCwS0pClTT7lU/ZDzUYhrSAuu7mCFjKnNEWerBmq5oM8gwNVU8Zs0V5YawhauhfFo/ftq1MHr5ArY4Ul";

            Spire.License.LicenseProvider.SetLicenseKey(spireKey);

            string treatmentWorkbookPath = "C:\\dl\\P106071-06_Bonavista_Falher_-12-02-_102_13-01-044-01W5_N2_Foamed_ELE-Clean_Ball_Drop_Workbook_(72881-1).xlsx"; //todo: find most recent treatment workbook on job

            Workbook workbook = new Workbook();
            workbook.LoadFromFile(treatmentWorkbookPath);


            string outstandingQ = "select * from postJobReportQueue where processed = 0 and errMsg is null";

            using (LocalDatabase ldb = new LocalDatabase(1))
            {
                


            }



            saveWorkSheet(workbook, "PF Cover", "cover.pdf");
            saveWorkSheet(workbook, "Treatment Report", "treatmentSchedule.pdf");


            //todo: pull from incoming job
            string jobID = "F3519";
            string customer = "Ridgeback";
            string surface = "(10-34)";
            string uwi = "100-07-10-064-19W5";
            string pdfName = customer + "_" + surface + "_" + uwi + "_Post_Frac_Report.pdf"; 

    

            //todo: find all treatment chart zips on a job and extract them
            string treatmentReportZipPath = @"c:\projects\" + jobID + ".zip";
            string extractPath = @"c:\projects\extract\" + jobID;


            ZipFile.ExtractToDirectory(treatmentReportZipPath, extractPath);


           PdfDocument pdf = new PdfDocument(new PdfWriter(pdfName));
            PdfMerger merger = new PdfMerger(pdf);

            //Add pages from the first document
            PdfDocument firstSourcePdf = new PdfDocument(new PdfReader("cover.pdf"));
            merger.Merge(firstSourcePdf, 1, firstSourcePdf.GetNumberOfPages());


            //Add pages from the second pdf document
            PdfDocument secondSourcePdf = new PdfDocument(new PdfReader("treatmentSchedule.pdf"));
            merger.Merge(secondSourcePdf, 1, secondSourcePdf.GetNumberOfPages());

            DirectoryInfo d = new DirectoryInfo(@"c:\projects\extract\" + jobID);
            foreach (var file in d.GetFiles())
            {

                PdfDocument pdfExtract = new PdfDocument(new PdfReader(file.FullName));
                merger.Merge(pdfExtract, 1, pdfExtract.GetNumberOfPages());
                pdfExtract.Close();

            }


            firstSourcePdf.Close();
            secondSourcePdf.Close();
            pdf.Close();
            merger.Close();

            //todo: attach this to the attachment tab on job with _1 _2 version




            Directory.Delete(extractPath, true);

        }

        private static void saveWorkSheet(Workbook workbook, string sheetName,string saveAs)
        {
            Worksheet sheet = (Worksheet)workbook.Worksheets.Where(x => x.Name == sheetName).FirstOrDefault();
            MemoryStream stream = new MemoryStream();

            sheet.SaveToPdfStream(stream);

            using (FileStream file = new FileStream(saveAs, FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                file.Write(bytes, 0, bytes.Length);
                stream.Close();
            }


        }
    }
}